#include <stdio.h>
#include <wiringPi.h>
#include <stdbool.h>
#include <string.h>

#define	OUT1  1
#define OUT2  4
#define OUT3  5
#define OUT4  6 
#define IN1   29
#define IN2   28
#define IN3   27
#define IN4   26	

// Function declaration
static void pininit(void);
int check_rows(void); 
int check_keypad(void);


int main (void)
{  
pininit();
 
bool ingedrukt = false;
 


while (1) {


int key=check_keypad();
if (key != 0){
if (ingedrukt == false){
printf("\n %d",key); 

ingedrukt = true;

}
else {
ingedrukt = false; 
}
}
}

return 0 ;
}

void pininit(void) {

wiringPiSetup () ;
pinMode (OUT1, OUTPUT);
pinMode (OUT2, OUTPUT);
pinMode (OUT3, OUTPUT);
pinMode (OUT4, OUTPUT);
pinMode (IN1, INPUT);
pinMode (IN2, INPUT);
pinMode (IN3, INPUT);
pinMode (IN4, INPUT);

}

int check_rows(void) {
delay (10);
if (digitalRead(29))
return 1;

if (digitalRead(28))
return 2;

if (digitalRead(27))
return 3;

if (digitalRead(26))
return 4;

return 0;
}

int check_keypad(void){

int row=0;

digitalWrite (1, HIGH);
row=check_rows();
digitalWrite (1, LOW);
if(row != 0) {
return (row-1)*4+1;
}

digitalWrite (4, HIGH);
row=check_rows();
digitalWrite (4, LOW);
if(row != 0) {
return (row-1)*4+2;
}

digitalWrite (5, HIGH);
row=check_rows();
digitalWrite (5, LOW);
if(row != 0) {
return (row-1)*4+3;
}

digitalWrite (6, HIGH);
row=check_rows();
digitalWrite (6, LOW);
if(row != 0) {
return (row-1)*4+4;
}

return 0;

}
